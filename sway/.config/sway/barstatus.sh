#!/bin/bash

time="$(date +%H.%M)"
battery_percentage="$(</sys/class/power_supply/BAT0/capacity)%"

echo  ☇ $battery_percentage '|' $time
