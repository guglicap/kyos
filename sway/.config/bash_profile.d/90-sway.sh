# Fix Java apps
export AWT_TOOLKIT=MToolkit

if systemctl -q is-active graphical.target && [[ "$(tty)" = "/dev/tty1" ]]; then
	systemctl --user import-environment
	systemctl --user start sway.service
fi
