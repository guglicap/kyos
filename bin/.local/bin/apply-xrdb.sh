#!/bin/bash

userresources="${HOME}/.config/Xresources.d"

[[ -d "${userresources}" ]] && {
	for res in "${userresources}"/* ; do
		xrdb -merge "$res"
	done
}
