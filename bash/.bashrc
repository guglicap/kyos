#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

[[ -d "${HOME}/.config/bashrc.d" ]] && {
	for file in "${HOME}/.config/bashrc.d"/*; do
		source "$file"
	done
}

PS1='\W \$ '

#export R_ENVIRON_USER=${HOME}/.config/r/Renviron
