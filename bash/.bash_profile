#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

[[ -d ~/.config/bash_profile.d ]] && {
	for f in ~/.config/bash_profile.d/* ; do
		source "$f"
	done
}
